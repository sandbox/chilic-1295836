(function ($) {
  if (Drupal.jsEnabled){
    $(document).ready(function(){
      function init_masha(){
        MaSha.instance = new MaSha(Drupal.settings.masha);
      }

      if (window.addEventListener){
        window.addEventListener('load', init_masha);
      }
      else {
        window.attachEvent('onload', init_masha);
      }
    });
  }
})(jQuery);