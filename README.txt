CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

MASHA (short for Mark & Share) is a JavaScript utility
allowing you to mark interesting parts of web page content and share it.
Just select text (paragraphs, sentences, words) on MASHA powered page and copy
generated URL from location bar.


INSTALLATION
------------

1. Copy the masha module directory to your sites/all/modules
    or sites/SITENAME/modules directory.

2. Download masha library from http://mashajs.com/#download

3. Copy the masha library directory from archive to sites/all/libraries/masha
    or sites/<domain>/libraries/masha directory.

4. Enable the module at Administer >> Site building >> Modules.

5. Visit masha settings page http://SITENAME/admin/settings/masha
